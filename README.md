## Deployment notes

```sh
git clone https://gitlab.com/form2i/express-socket-canvas.git

cd express-socket-canvas

npm i

pm2 start app.js
```

add nginx server config

```
server {
    listen       80;
    listen       [::]:80;
    server_name  draw.mirreal.net;
    root         /usr/share/nginx/html;

    include /etc/nginx/default.d/*.conf;

    error_page 404 /404.html;
    location = /404.html {
    }

    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
    }

    location / {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $http_host;
        proxy_pass http://127.0.0.1:7720;
    }
}
```
